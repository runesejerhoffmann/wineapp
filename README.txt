DOCUMENTATION
-------------


INTRODUCTION
-------------
In this readme i will describe my used tools for the case,
furthermore i will describe my process throughout the case.


USED TOOLS
-------------
   * Gulp (https://www.npmjs.com/package/gulp)
   For building my project.

   * Typescript (https://www.typescriptlang.org/)
   Adding the possibility to have types, and to extend
   the possibilities of javascript.

   * jQuery (https://jquery.com/)
   For comfort, writing less code to achieve simple tasks.

   * KnockoutJS (http://knockoutjs.com/)
   As my MV library.


PROCESS
-------------
First of all, i read the case and gave myself a day to think about how to
build the app. i thought about the best tools for the job and i looked
at vivino (an app that reminds alot of the case) and dribble (for color and UI inspiration).

I began from scratch setting up a solution with my chosen tools i then styled the UI
and began implementing knockout. 

I ran into some troubles saving my array to localstorage so i went over the 4 hours the case described. 

None the less i got the things i wanted up and running. 
such ass: adding new wine, editing score and comment(a little problem with an empty object getting pushed to the array), 
deleting, and most importantly, the function to save and extract your wines from
localstorage.

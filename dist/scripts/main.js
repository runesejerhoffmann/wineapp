var vineApp;
(function (vineApp) {
    var Bindings;
    (function (Bindings) {
        var ClickToggleParentClass = (function () {
            function ClickToggleParentClass() {
            }
            ClickToggleParentClass.prototype.init = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                if (!bindingContext)
                    return;
                var value = valueAccessor();
                var activeClassName = value.class || 'active';
                ko.utils.registerEventHandler(element, "click", function () {
                    if ($(element).parent().hasClass(activeClassName)) {
                        $(element).parent().removeClass(activeClassName);
                    }
                    else {
                        $(element).parent().addClass(activeClassName);
                    }
                });
            };
            return ClickToggleParentClass;
        }());
        Bindings.ClickToggleParentClass = ClickToggleParentClass;
        ko.bindingHandlers['clickToggleParentClass'] = new ClickToggleParentClass();
    })(Bindings = vineApp.Bindings || (vineApp.Bindings = {}));
})(vineApp || (vineApp = {}));



/// <reference path="../../../definitions/knockout.d.ts" />
/// <reference path="../../../definitions/jquery.d.ts" />
var vineApp;
(function (vineApp) {
    var ViewModels;
    (function (ViewModels) {
        var VineAppMasterViewModel = (function () {
            function VineAppMasterViewModel() {
                var _this = this;
                this.vineToAddName = ko.observable("");
                this.vineToAddDetails = ko.observable("");
                this.vineToAddScore = ko.observable("");
                this.vineToAddComment = ko.observable("");
                this.name = ko.observable("");
                this.details = ko.observable("");
                this.score = ko.observable("");
                this.comment = ko.observable("");
                this.vines = ko.observableArray([]);
                this.arrayIsPopulated = ko.observable(false);
                this.addToArray = function () {
                    _this.vines.push({ Name: _this.vineToAddName(), Details: _this.vineToAddDetails(), Score: _this.vineToAddScore(), Comment: _this.vineToAddComment() });
                    var arrayToJSON = ko.toJSON(_this.vines());
                    _this.saveVinesToLocalStorage();
                };
                this.getVinesFromLocalStorage = function () {
                    if (_this.vines() == null) {
                        _this.arrayIsPopulated(false);
                        //The array from localstorage is empty 
                        //We therefor feed our array with "dummy" data for showing
                        _this.vines([{ Name: "Prosecco Miol Bortolomiol", Details: "A prosecco whose natural determination all but vibrates. In expression it is essential yet elegant thanks to the soft aromas it offers. The fine dense bubbles give the palate a soft and seductive caress. It’s a wine whose allure is its feminine whisper and the gorgeous balance that tingles with fresh hints of citrus fruit and musk.", Score: "Good", Comment: "My favorite" }, { Name: "Cannonau i FIori", Details: "Cannonau is the main red grape of Sardinia. The DOC Cannonau is one of the most well-known. It's a flower that grows here in our vineyards... that's why we called I Fiori in range with the other classical traditinal wine varietarls of Sardinia. The flower on the label is a Aquilegia Nuragica, one of the two hundred endemic species of Sardinia, a symbol of its diversity. It grows in the Gorroppu canyon (Gennargentu National Park) and there are only ten plants left and protected.", Score: "okay", Comment: "Good with fish" }]);
                    }
                    else {
                        _this.arrayIsPopulated(true);
                        //The array from localstorage is populated
                        //We feed our array with extracted data
                        _this.vines(JSON.parse(localStorage.getItem("vines")));
                    }
                };
                this.saveVinesToLocalStorage = function () {
                    var arrayToJSON = ko.toJSON(_this.vines());
                    localStorage.setItem('vines', arrayToJSON);
                };
                this.addChangesToArray = function () {
                    //This function pushes an empty object (except score)
                    //into the array, this is of cause not the intention
                    _this.vines.push({ Name: _this.name(), Details: _this.details(), Score: _this.score(), Comment: _this.comment() });
                    _this.saveVinesToLocalStorage();
                };
                this.removeVine = function (data) {
                    _this.vines.remove(data);
                    _this.saveVinesToLocalStorage();
                };
                this.vines(JSON.parse(localStorage.getItem("vines")));
                this.getVinesFromLocalStorage();
            }
            return VineAppMasterViewModel;
        }());
        ViewModels.VineAppMasterViewModel = VineAppMasterViewModel;
        ko.applyBindings(new VineAppMasterViewModel());
    })(ViewModels = vineApp.ViewModels || (vineApp.ViewModels = {}));
})(vineApp || (vineApp = {}));

﻿module vineApp.Bindings {
    export interface IClickToggleParentClassArgs {
        class: string;
    }
    export class ClickToggleParentClass {
        public init(element: any, valueAccessor: () => IClickToggleParentClassArgs, allBindingsAccessor?: KnockoutAllBindingsAccessor, viewModel?: any, bindingContext?: KnockoutBindingContext) {
            if (!bindingContext)
                return;

            var value = valueAccessor();
            var activeClassName = value.class || 'active';

            ko.utils.registerEventHandler(element, "click", function () {

                if ($(element).parent().hasClass(activeClassName)) {
                    $(element).parent().removeClass(activeClassName);
                } else {
                    $(element).parent().addClass(activeClassName); 
                }
                 
            }); 
        }
    }
    ko.bindingHandlers['clickToggleParentClass'] = new ClickToggleParentClass();   
} 
﻿/// <reference path="../../../definitions/knockout.d.ts" />
/// <reference path="../../../definitions/jquery.d.ts" />

module vineApp.ViewModels {
    export class VineAppMasterViewModel {
        public vineToAddName = ko.observable("");
        public vineToAddDetails = ko.observable("");
        public vineToAddScore = ko.observable("");
        public vineToAddComment = ko.observable("");
        public name = ko.observable("");
        public details = ko.observable("");
        public score = ko.observable("");
        public comment = ko.observable("");
        public vines = ko.observableArray([]);
        public arrayIsPopulated = ko.observable(false);

        constructor() {
            this.vines(JSON.parse(localStorage.getItem("vines")));
            this.getVinesFromLocalStorage();
        }

        public addToArray = () => {
            this.vines.push({ Name: this.vineToAddName(), Details: this.vineToAddDetails(), Score: this.vineToAddScore(), Comment: this.vineToAddComment() });
            var arrayToJSON = ko.toJSON(this.vines());
            this.saveVinesToLocalStorage();
        }

        public getVinesFromLocalStorage = () => {
            if (this.vines() == null) {
                this.arrayIsPopulated(false);
                //The array from localstorage is empty 
                //We therefor feed our array with "dummy" data for showing
                this.vines([{ Name: "Prosecco Miol Bortolomiol", Details: "A prosecco whose natural determination all but vibrates. In expression it is essential yet elegant thanks to the soft aromas it offers. The fine dense bubbles give the palate a soft and seductive caress. It’s a wine whose allure is its feminine whisper and the gorgeous balance that tingles with fresh hints of citrus fruit and musk.", Score: "Good", Comment: "My favorite" }, { Name: "Cannonau i FIori", Details: "Cannonau is the main red grape of Sardinia. The DOC Cannonau is one of the most well-known. It's a flower that grows here in our vineyards... that's why we called I Fiori in range with the other classical traditinal wine varietarls of Sardinia. The flower on the label is a Aquilegia Nuragica, one of the two hundred endemic species of Sardinia, a symbol of its diversity. It grows in the Gorroppu canyon (Gennargentu National Park) and there are only ten plants left and protected.", Score: "okay", Comment: "Good with fish" }]);
            } else {
                this.arrayIsPopulated(true);
                //The array from localstorage is populated
                //We feed our array with extracted data
                this.vines(JSON.parse(localStorage.getItem("vines")));
            }
        }

        public saveVinesToLocalStorage = () => {
            var arrayToJSON = ko.toJSON(this.vines());
            localStorage.setItem('vines', arrayToJSON);
        }

        public addChangesToArray = () => {
            //This function pushes an empty object (except score)
            //into the array, this is of cause not the intention
            this.vines.push({ Name: this.name(), Details: this.details(), Score: this.score(), Comment: this.comment() });
            this.saveVinesToLocalStorage();
            
        }

        public removeVine = (data) => {
            this.vines.remove(data);
            this.saveVinesToLocalStorage();
        }

    }
    ko.applyBindings(new VineAppMasterViewModel());
}